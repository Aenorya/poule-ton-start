﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Poule Simulator/Mushroom Data", fileName ="Mushroom Data")]
public class MushroomData : ScriptableObject
{
    public string variety;
    public string description;
    public int value = 0;
    public List<GameEvent> eventsToRaise = new List<GameEvent>();

    public void RaiseAllEvents()
    {
        foreach(GameEvent gEvent in eventsToRaise)
        {
            gEvent.Notify(value);
        }
    }
}
