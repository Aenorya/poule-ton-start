﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Poule Simulator/States/Suspicious", fileName = "Suspicious")]

public class SuspiciousState : BaseState
{
    private bool hasLostPlayer = false;

    public float startTime;
    public float delayBeforeChase;

    public BaseState chaseState;
    public BaseState patrolState;
    public GameObject chicken;

    public override void OnStateBegun(StateMachine machine)
    {
        base.OnStateBegun(machine);
        startTime = Time.time;
        hasLostPlayer = false;
    }

    public override void OnLoss(GameObject lost)
    {
        if (lost.CompareTag("Player"))
        {
            hasLostPlayer = true;
        }
    }

    public override void OnSight(GameObject seen)
    {
        //startTime = Time.time;
        chicken = seen;
        hasLostPlayer = false;
    }

    public override void OnUpdate()
    {
        Debug.Log(Time.time - startTime + " / " + delayBeforeChase);
        if (Time.time - startTime > delayBeforeChase)
        {
            if (!hasLostPlayer)
            {
                TransitionTo(chaseState);
            }
            else
            {
                TransitionTo(patrolState);
            }
        }
        stateMachine.attachedNPC.agent.transform.LookAt(chicken.transform);
    }
}
