﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SightRange : MonoBehaviour
{
    public NPC callback;
    public Color normalColor, detectedColor;

    public void Start()
    {
        callback = GetComponentInParent<NPC>();
        GetComponent<Renderer>().material.color = normalColor;

    }
    public void OnChickenSpotted(GameObject collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            callback.ChickenAlert(collision.gameObject);
            GetComponent<Renderer>().material.color = detectedColor;
        }
    }

    public void OnChickenLost(GameObject collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GetComponent<Renderer>().material.color = normalColor;
            callback.ChickenLost(collision);
        }
    }
}
