﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Poule Simulator/States/Attack", fileName = "Attack")]
public class AttackState : BaseState
{
    public BaseState chaseState;
    public BaseState idleState;

    public override void OnCollisionStopped(GameObject collisioned)
    {
        if (collisioned.CompareTag("Player"))
        {
            TransitionTo(chaseState);
        }
    }
}
