﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StateMachine
{
    public BaseState currentState;
    public NPC attachedNPC;
    public StateMachine(NPC npc, BaseState initialState)
    {
        attachedNPC = npc;
        TransitionTo(initialState);
    }

    public void TransitionTo(BaseState nextState)
    {
        currentState = nextState;
        currentState.OnStateBegun(this);
    }

}
