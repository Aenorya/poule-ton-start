﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Poule Simulator/States/Patrol", fileName = "Patrol")]
public class PatrolState : BaseState
{
    public BaseState suspiciousState;
    public override void OnStateBegun(StateMachine machine)
    {
        base.OnStateBegun(machine);
        NextPatrolStep();
    }

    public override void OnSight(GameObject collisioned)
    {
        if(collisioned.tag == "Player")
        {
            TransitionTo(suspiciousState);
        }
    }

    public override void OnCollision(GameObject collisioned)
    {
        if (collisioned == stateMachine.attachedNPC.patrolRoute[stateMachine.attachedNPC.currentPatrolStep])
        {
            NextPatrolStep();
        }
    }

    public void NextPatrolStep()
    {
        stateMachine.attachedNPC.currentPatrolStep = (stateMachine.attachedNPC.currentPatrolStep + 1) % stateMachine.attachedNPC.patrolRoute.Count;
        stateMachine.attachedNPC.agent.SetDestination(stateMachine.attachedNPC.patrolRoute[stateMachine.attachedNPC.currentPatrolStep].transform.position);
    }

    public override void TransitionTo(BaseState nextState)
    {
        stateMachine.attachedNPC.agent.ResetPath();
        base.TransitionTo(nextState);
    }

}
