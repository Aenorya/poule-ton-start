﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseState : ScriptableObject
{
    public StateMachine stateMachine;
    public string animationName;
    public virtual void OnStateBegun(StateMachine machine)
    {
        stateMachine = machine;
        stateMachine.attachedNPC.animator.Play(animationName);
    }
    public virtual void TransitionTo(BaseState nextState)
    {
        stateMachine.TransitionTo(nextState);
    }
    public virtual void OnCollision(GameObject collisioned)
    {

    }

    public virtual void OnCollisionStopped(GameObject collisioned)
    {

    }

    public virtual void OnSight(GameObject seen)
    {

    }

    public virtual void OnLoss(GameObject lost)
    {

    }

    public virtual void OnUpdate()
    {

    }
}
