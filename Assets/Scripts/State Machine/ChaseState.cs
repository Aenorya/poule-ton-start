﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(menuName = "Poule Simulator/States/Chase", fileName = "Chase")]

public class ChaseState : BaseState
{
    public BaseState idleState;
    public BaseState attackState;
    public GameObject chickenTarget;
    public float maxDistance;
    public override void OnStateBegun(StateMachine machine)
    {
        base.OnStateBegun(machine);
        chickenTarget = GameObject.FindObjectOfType<ChickenController>().gameObject;
    }
    public void FollowChicken()
    {
        stateMachine.attachedNPC.agent.SetDestination(chickenTarget.transform.position);
    }
    public override void OnUpdate()
    {
        FollowChicken();
        if (Vector3.Distance(chickenTarget.transform.position, stateMachine.attachedNPC.transform.position) > maxDistance)
        {
            TransitionTo(idleState);
        }
    }
    public override void OnCollision(GameObject collisioned)
    {
        if (collisioned.CompareTag("Player"))
        {
            TransitionTo(attackState);
        }
    }
    public override void TransitionTo(BaseState nextState)
    {
        stateMachine.attachedNPC.agent.ResetPath();
        base.TransitionTo(nextState);
    }
}
