﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Poule Simulator/States/Idle", fileName = "Idle")]
public class IdleState : BaseState
{
    public BaseState patrolState;
    public BaseState chaseState;

    public override void OnStateBegun(StateMachine machine)
    {
        base.OnStateBegun(machine);
        stateMachine.attachedNPC.StartCoroutine(WaitFor(3f, 5f));
    }

    public IEnumerator WaitFor(float minSeconds, float maxSeconds)
    {
        yield return new WaitForSeconds(Random.Range(minSeconds, maxSeconds));
        TransitionTo(patrolState);
    }
}
