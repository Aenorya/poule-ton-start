﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu (menuName = "Poule Simulator/Game Event", fileName ="Game Event")]
public class GameEvent : ScriptableObject
{
    private List<IGameEventListener> listeners = new List<IGameEventListener>();

    public void Subscribe(IGameEventListener newListener)
    {
        listeners.Add(newListener);
    }

    public void UnSubscribe(IGameEventListener oldListener)
    {
        listeners.Remove(oldListener);
    }

    public void Notify()
    {
        foreach(IGameEventListener listener in listeners)
        {
            listener.Notify();
        }
    }

    public void Notify(int value)
    {
        foreach (IGameEventListener listener in listeners)
        {
            listener.Notify(value);
        }
    }
}
