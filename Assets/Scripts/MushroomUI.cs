﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MushroomUI : MonoBehaviour, IGameEventListener
{
    public int mushroomsAtStart = 0, mushroomsNow = 0;
    public GameEvent catchAMushroom;
    void Start()
    {
        catchAMushroom.Subscribe(this);
        mushroomsAtStart = FindObjectsOfType<Mushroom>().Length;
        UpdateUI();
    }


    void UpdateUI()
    {
        GetComponent<TextMeshProUGUI>().text = string.Format("{0} / {1}", mushroomsNow, mushroomsAtStart);
    }

    public void Notify()
    {
        mushroomsNow++;
        UpdateUI();
    }

    public void Notify(int value)
    {
        mushroomsNow+=value;
        UpdateUI();
    }
}
