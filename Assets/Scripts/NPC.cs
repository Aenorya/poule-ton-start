﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPC : MonoBehaviour
{
    public StateMachine fsm;
    public BaseState initialState;
    public Animator animator;
    public NavMeshAgent agent;
    public List<GameObject> patrolRoute;
    public int currentPatrolStep = -1;

    public void Start()
    {
        fsm = new StateMachine(this, initialState);
    }

    public void ChickenAlert(GameObject chicken)
    {
        fsm.currentState.OnSight(chicken);
    }

    public void ChickenLost(GameObject chicken)
    {
        fsm.currentState.OnLoss(chicken);
    }

    public void Update()
    {
        fsm.currentState.OnUpdate();
    }
    
    public void OnCollisionEnter(Collision collision)
    {
        fsm.currentState.OnCollision(collision.gameObject);
    }

    public void OnCollisionExit(Collision collision)
    {
        fsm.currentState.OnCollisionStopped(collision.gameObject);
    }
}
