﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenController : MonoBehaviour
{
    public float speed = 5;
    Rigidbody rb;
    public GameObject eggPrefab;
    public Transform eggSpawn;

    private void Start()
    {
        rb = GetComponentInChildren<Rigidbody>();
    }
    void FixedUpdate()
    {
        Vector3 direction = new Vector3(0,0,0);
        if (Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.UpArrow))
        {
            direction += Vector3.forward * speed * Time.fixedDeltaTime;
        }
        if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.LeftArrow))
        {
            direction += Vector3.left * speed * Time.fixedDeltaTime;
        }
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            direction += Vector3.back * speed * Time.fixedDeltaTime;
        }
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            direction += Vector3.right * speed * Time.fixedDeltaTime;
        }

        rb.MovePosition(transform.position + direction);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Mushroom"))
        {
            other.GetComponent<Mushroom>().data.RaiseAllEvents();
            GameObject egg = Instantiate(eggPrefab);
            egg.transform.position = eggSpawn.position;
            Destroy(other.gameObject);
        }
        if (other.CompareTag("Sight"))
        {
            other.GetComponent<SightRange>().OnChickenSpotted(gameObject);
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Sight"))
        {
            other.GetComponent<SightRange>().OnChickenLost(gameObject);
        }
    }
}
